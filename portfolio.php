<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Портфолио</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<div class="container portfolio-page">
		<div class="row">
			<ul class="nav navbar-nav">
			  <li role="presentation"><a href="index.php">Главная</a></li>
			  <li role="presentation" class="active"><a href="portfolio.php">Портфолио</a></li>
			  <li role="presentation"><a href="contacts.php">Контакты</a></li>
			</ul>
		</div>
	</div>
	<div class="container index-page">
		<div class="row">
			<div class="col-lg-4 portfolio-foto"><img class="img-rounded" src="img/portfolio-1.png" alt="img1" alt=""></div>
			<div class="col-lg-8 text">
				<h2>Сайт компании ЕДЦ, Москва</h2>
				<p>Рассмотреть <a href="http://edc.ru/ru">подробнее</a></p>
				<p>Для этого сайта мною были выполнены:</p>
				<ul>
					<li>Верстка адаптивной версии сайта под мобильное, планшетное и десктопные разрешения</li>
					<li>Создание нескольких типов материалов посредством работы в админ-панели Друпала</li>
					<li>Перенос сайта с мультисайтинговой версии Друпала на отдельный движок, были некоторые нюансы</li>
					<li>Изменен адрес страницы регистрации и логина, на стандартную страницу поставлен редирект на 404.php через .htaccess</li>
					<li>Постоянно выполняются текущые мелкие задачи по проекту, т.к. заказчик выдумщик =)</li>
				</ul>
				<p>Это был первый для меня опыт адаптивной верстки. Сайт довольно типичный по исполнению, но часто возникают непредвиденные трудности из-за того, что проект старый, над ним работало 5 команд разработчиков в разные времена, костылей вставлено столько, что год крематорий ими топить.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4 portfolio-foto"><img class="img-rounded" src="img/portfolio-2.png" alt="img2" alt=""></div>
			<div class="col-lg-8 text">
				<h2>Сайт компании "Инсайт", Омск</h2>
				<p>Рассмотреть <a href="demo11.iaglobus.ru">подробнее</a></p>
				<p>Сайт был сверстан и собран на движке Друпал с нуля лично мной в рамках работы над ним были выполнены следующие задачи:</p>
				<ul>
					<li>Формирование сборки модулей Друпал для текущей задачи</li>
					<li>Создание всех необходимых материалов, страниц, типов отображения материалов</li>
					<li>Верстка полученных страниц</li>
					<li>Наполнение сайта текстами и фотоматериалами</li>
					<p>Это был первый для меня проект, в нем я нашел много первых для себя граблей. В ходе работы были получены многие навыки работы с друпал, освоено много материалов как по друпал, так и по верстке. Сайт пока находится на демо хостинге, ожидается скорый запуск в продакшн</p>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4 portfolio-foto"><img class="img-rounded" src="img/portfolio-3.png" alt="img3" alt=""></div>
			<div class="col-lg-8 text">
				<h2>СТО Август, Омск</h2>
				<p>Рассмотреть <a href="http://demo6.iaglobus.ru/">подробнее</a></p>
				<p>Сайт начинал делать не я, мной на нем выполнена верстка только одной страницы, для нее созданы вебформы заказа, верстка выполнена в адаптивном варианте</p>
			</div>
		</div>
		
		</div>
	</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>