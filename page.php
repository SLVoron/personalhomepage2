
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Контакты</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="css/style.css">
</head>
</head>
<body>
	<div class="container index-page">
		<div class="row">
			<ul class="nav navbar-nav">
			  <li role="presentation" class="active"><a href="index.php">Главная</a></li>
			  <li role="presentation"><a href="portfolio.php">Портфолио</a></li>
			  <li role="presentation"><a href="contacts.php">Контакты</a></li>
			  <li role="presentation"><a href="page.php">Страница</li>
			</ul>
		</div>
	</div>
	<div class="container index-page">
		<div class="row">
			<div class="col-lg-5"><img src="img/Iam.jpg" alt="my-foto"></div>
			<div class="col-lg-7">
				<h2>Это Я! =)</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur sed itaque debitis quos incidunt, omnis neque alias perspiciatis inventore cupiditate quidem impedit hic. Recusandae vitae pariatur magnam ab explicabo dignissimos molestias commodi, eveniet. Magni maxime sint eos earum voluptate voluptatem minus ducimus quibusdam, nobis rem voluptas nam odio voluptates accusamus odit! Mollitia, explicabo soluta ex temporibus a dignissimos blanditiis quo quisquam quibusdam itaque animi vel dolores fugiat beatae at adipisci expedita saepe commodi deleniti. Quasi sapiente, assumenda cum deserunt consequuntur temporibus iusto, distinctio reiciendis aperiam quidem veniam repellat consectetur minima minus! Consequuntur odit, quam fugiat repellendus dolores libero, molestias maxime?</p>
			</div>
		</div>
	</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>

</body>
</html>