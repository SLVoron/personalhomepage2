<?php
$realm = 'ia tebya ne znayu =(';
$users = array('admin' => 'qwerty');


if (empty($_SERVER['PHP_AUTH_DIGEST'])) {
    header('HTTP/1.1 401 Unauthorized');
    header('WWW-Authenticate: Digest realm="'.$realm.
           '",qop="auth",nonce="'.uniqid().'",opaque="'.md5($realm).'"');

    die('Нет пароля - нет админки =P');
}

if (!($data = http_digest_parse($_SERVER['PHP_AUTH_DIGEST'])) ||
    !isset($users[$data['username']]))
    die('Неправильные данные!');

$A1 = md5($data['username'] . ':' . $realm . ':' . $users[$data['username']]);
$A2 = md5($_SERVER['REQUEST_METHOD'].':'.$data['uri']);
$valid_response = md5($A1.':'.$data['nonce'].':'.$data['nc'].':'.$data['cnonce'].':'.$data['qop'].':'.$A2);

if ($data['response'] != $valid_response)
    die('Неправильные данные!');

function http_digest_parse($txt)
{
    // защита от отсутствующих данных
    $needed_parts = array('nonce'=>1, 'nc'=>1, 'cnonce'=>1, 'qop'=>1, 'username'=>1, 'uri'=>1, 'response'=>1);
    $data = array();
    $keys = implode('|', array_keys($needed_parts));

    preg_match_all('@(' . $keys . ')=(?:([\'"])([^\2]+?)\2|([^\s,]+))@', $txt, $matches, PREG_SET_ORDER);

    foreach ($matches as $m) {
        $data[$m[1]] = $m[3] ? $m[3] : $m[4];
        unset($needed_parts[$m[1]]);
    }

    return $needed_parts ? false : $data;
}

require('../vendor/slvoron/pdodbclass/db.php');
$start=$_GET[p] * 5;
$result = $db->query("SELECT * FROM messages LIMIT $start , 5")->fetchAll(PDO::FETCH_ASSOC);
$count = count($result);
$db_count = $db->query('SELECT COUNT(*) FROM messages')->fetchAll(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Контакты</title>
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="../css/style.css">
</head>
</head>
<body>
	<div class="container index-page">
		<div class="row">
			<ul class="nav navbar-nav">
			  <li role="presentation" class="active"><a href="/index.php">Главная</a></li>
			  <li role="presentation"><a href="/portfolio.php">Портфолио</a></li>
			  <li role="presentation"><a href="/contacts.php">Контакты</a></li>
			</ul>
		</div>
	</div>
	<div class="container index-page">
		<div class="row">
		<table class="table table-bordered">
			<tbody>
				<tr>
					<th>id</th>
					<th>Имя</th>
					<th>E-mail</th>
					<th>Сообщение</th>
					<th>Удалить?</th>
				</tr>
			<?php
			
			for ($i=0; $i<$count; $i++) {
				echo '<tr>
						<td>'.$result[$i][id].'</td>
						<td>'.$result[$i][name].'</td>
						<td>'.$result[$i][email].'</td>
						<td>'.$result[$i][message].'</td>
						<td><a href="delete-data.php?id='.$result[$i][id].'">Удалить запись</a></td>
					</tr>';
			}
			
			echo '<p>';			
			for ($j=0; $j<($db_count[0]['COUNT(*)']/5); $j++) 
			{
				if ($_GET[p]==$j||(!$_GET[p]&&$j==0)) echo "<strong>";
				echo '<a href="index.php?p='.$j.'"> '.($j+1).' </a>';
				if ($_GET[p]==$j||(!$_GET[p]&&$j==0)) echo "</strong>";
			}

			echo '</p>';
			 ?>
			</tbody>
		</table>
		</div>
	</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../js/bootstrap.min.js"></script>

</body>
</html>