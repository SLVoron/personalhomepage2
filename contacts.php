<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Контакты</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="css/style.css">
</head>
</head>
<body>
	<div class="container contacts-page">
		<div class="row">
			<ul class="nav navbar-nav">
			  <li role="presentation"><a href="index.php">Главная</a></li>
			  <li role="presentation"><a href="portfolio.php">Портфолио</a></li>
			  <li role="presentation" class="active"><a href="contacts.php">Контакты</a></li>
			</ul>
		</div>
	</div>
	<div class="container index-page">
		<div class="row">
			<div class="col-lg-12">
				<h2>Меня можно найти по ВКонтакте и Скайпе</h2>
				<p>Я <a href="vk.com/voronenkosl">ВКонтакте</a></p>
				<p>Мой скайп slvoronenko</p>
				<p>Также вы можете заполнить данную форму и написать мне сообщение, я его обязательно прочитаю и, возможно, отвечу вам =)</p>
			</div>
		</div>
<?php
require('vendor/slvoron/pdodbclass/db.php');
if (!isset($_POST['submit']))
{
	$_POST['name'] = '';
	$_POST['email'] = '';
	$_POST['message'] = '';
}
else 
{
	$email = $_POST['email'];
	$message = $_POST['message'];
	$valid_error = array();

	$email = clean($email);
	$message = clean($message);

	if(!empty($email) && !empty($message)) {
		$email_validate = filter_var($email, FILTER_VALIDATE_EMAIL); 

		if(check_length($message, 2, 500) && $email_validate) {
			$fp=fopen('messages.csv', 'a');
			$name = $_POST['name'];
			$email = $_POST['email'];
			$message = $_POST['message'];
			$text = '"'.$name.'", "'.$email.'", "'.$message.'", ';
			$write = fwrite($fp, $text);
			fclose($fp);
			$db->query("INSERT INTO messages VALUES ('','".$_POST['name']."','".$_POST["email"]."','".$_POST["message"]."')");
			echo ('<p>Ваши данные успешно отправлены</p>');
		} else { echo "Введенные данные некорректные";}
	} else {echo "Заполните пустые поля";}

}
function clean($value = "") {
    $value = trim($value);
    $value = stripslashes($value);
    $value = strip_tags($value);
    $value = htmlspecialchars($value);
    
    return $value;
}
function check_length($value = "", $min, $max) {
    $result = (mb_strlen($value) < $min || mb_strlen($value) > $max);
    return !$result;
}
?>
		<div class="row">
			<div class="col-lg-6 col-lg-offset-3">
				<form action="contacts.php" method="POST">
					<div class="form-group">
						<div class="form-group">
							<label for="name">Имя:</label>
							<input type="text" name="name" class="form-control" id="name" placeholder="Введите ваше имя">
						</div>
						<div class="form-group">
							<label for="email">E-mail</label>
							<input type="email" name="email" class="form-control" id="email" placeholder="Введите ваш e-mail">
							<span><? ?></span>
						</div>
						<div class="form-group">
							<label for="message">Сообщение</label>
							<textarea name="message" id="message" cols="30" rows="10" placeholder="Введите ваше сообщение"></textarea>
							<span><?=@$valid_error['message']?></span>
						</div>
						<button type="submit" name="submit" class="btn btn-default">Отправить</button>
					</div>
				</form>
			</div>
		</div>

	</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>

</body>
</html>